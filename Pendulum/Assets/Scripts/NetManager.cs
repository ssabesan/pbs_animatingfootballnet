using UnityEngine;

namespace PhysicalBasedSimulation
{
    [System.Serializable]
    public class NetManager : MonoBehaviour
    {
        [Header("Net Paramters")]
        [SerializeField, Range(1, 10)]
        private float m_Height = 2.5f;
        [SerializeField, Range(1, 10)]
        private float m_Length = 7.5f;
        [SerializeField, Range(1, 100)]
        private int m_Rows = 5, m_Columns = 5;
        [SerializeField, Range(0, 10)]
        private int m_ParticlePerConnection = 0;
        [SerializeField]
        private bool m_FixTop = false, m_FixBottom = false, m_FixLeft = true, m_FixRight = true;

        [Header("Particle Paramters")]
        [SerializeField]
        private bool m_enableParticleMesh = true;
        [SerializeField, Range(0, 1)]
        private float m_ParticleSize = 0.2f;
        [SerializeField, Range(0, 1000)]
        private float m_NetMass = 1;
        [SerializeField, Range(1, 100)]
        private int m_KnotWeight = 1;
        [SerializeField, Range(1, 2)]
        private float m_LengthMultiplier = 1.0f;

        [Header("Prefabs")]
        [SerializeField]
        private PBDManager m_PBDManagerPrefab;
        [SerializeField]
        private Particle m_ParticlePrefab, m_ParticleWithoutMeshPrefab;
        [SerializeField]
        private ParticleConnection m_ConnectionPrefab;

        [HideInInspector]
        public PBDManager PBDManager { get; private set; }

        public void OverrideNet()
        {
            if(PBDManager != null) {
                DestroyImmediate(PBDManager.gameObject, true);
            }
            GenerateNet();
        }

        public void GenerateNet()
        {
            if (m_PBDManagerPrefab == null) {
                Debug.LogError("PBDManager Prefab is not set!");
                return;
            }
            else if (m_ParticlePrefab ==  null) {
                Debug.LogError("Particle Prefab is not set!");
                return;
            }
            else if (m_ConnectionPrefab == null) {
                Debug.LogError("Connection Prefab is not set!");
                return;
            }

            PBDManager = Instantiate(m_PBDManagerPrefab, transform);
            PBDManager.name = "Net";

            GameObject particleHolder = new("Particles");
            particleHolder.transform.SetParent(PBDManager.transform, false);

            GameObject connectionHolder = new("Connections");
            connectionHolder.transform.SetParent(PBDManager.transform, false);

            int numRows = m_Rows + m_ParticlePerConnection * (m_Rows - 1);
            int numCols = m_Columns + m_ParticlePerConnection * (m_Columns - 1);

            float rowSpacing = m_Height / (numRows - 1.0f);
            float colSpacing = m_Length / (numCols - 1.0f);
            Particle[,] particles = new Particle[numRows,numCols];
            float particleMass = m_NetMass / (numRows * numRows + m_Rows * m_Columns * (m_KnotWeight - 1));
            float knotParticleMass = particleMass * m_KnotWeight;

            for(int i = 0; i < numRows; i++) {
                GameObject rowParticleHolder, rowHorizontalConnectionHolder = null, rowVerticalConnectionHolder;
                bool isNodei = i % (m_ParticlePerConnection + 1) == 0;

                rowParticleHolder = new($"Particles - Row {i}");
                rowParticleHolder.transform.SetParent(particleHolder.transform, false);
                if(isNodei) {
                    rowHorizontalConnectionHolder = new($"Connections - Horizontal {i}");
                    rowHorizontalConnectionHolder.transform.SetParent(connectionHolder.transform, false);
                }
                rowVerticalConnectionHolder = new($"Connections - Vertical{i}");
                rowVerticalConnectionHolder.transform.SetParent(connectionHolder.transform, false);

                for (int j = 0; j < numCols; j++) {
                    if (j % (m_ParticlePerConnection + 1) != 0 && i % (m_ParticlePerConnection + 1) != 0)
                        continue;
                    
                    Particle particle = Instantiate(m_enableParticleMesh ? m_ParticlePrefab : m_ParticleWithoutMeshPrefab, rowParticleHolder.transform);
                    particle.name = $"Particle [{i}, {j}]";
                    particle.transform.SetLocalPositionAndRotation(new Vector3(j * colSpacing, -i  * rowSpacing), Quaternion.identity);
                    particle.transform.localScale = Vector3.one * m_ParticleSize;
                    
                    bool isNodej = j % (m_ParticlePerConnection + 1) == 0;
                    bool isKnot = isNodei && isNodej;
                    float mass = isKnot ? knotParticleMass : particleMass;
                    particle.OnCreate(isKnot && (
                        (m_FixTop && i == 0) ||
                        (m_FixBottom && i == numRows - 1) ||
                        (m_FixLeft && j == 0) || 
                        (m_FixRight && j == numCols - 1)
                    ));
                    particles[i, j] = particle;

                    if (i > 0 && isNodej) {
                        Particle p1 = particles[i - 1, j];
                        Particle p2 = particle;
                        ParticleConnection conn = Instantiate(m_ConnectionPrefab, rowVerticalConnectionHolder.transform);
                        conn.name = $"Connection: [{i - 1}, {j}] - [{i}, {j}]";
                        conn.OnCreate(p1, p2, m_LengthMultiplier);
                    }
                    if(j > 0 && isNodei) {
                        Particle p1 = particles[i, j - 1];
                        Particle p2 = particle;
                        ParticleConnection conn = Instantiate(m_ConnectionPrefab, rowHorizontalConnectionHolder.transform);
                        conn.name = $"Connection: [{i}, {j - 1}] - [{i}, {j}]";
                        conn.OnCreate(p1, p2, m_LengthMultiplier);
                    }
                }
            }
        }
    }

}
