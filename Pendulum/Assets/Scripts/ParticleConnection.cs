using Unity.Burst;
using UnityEngine;

namespace PhysicalBasedSimulation
{
    [BurstCompile, System.Serializable]
    public class ParticleConnection : MonoBehaviour
    {
        [SerializeField]
        private Particle m_Particle1, m_Particle2;
        [SerializeField, Range(1, 2)]
        private float m_LengthMultiplier;

        public Particle P1 => m_Particle1;
        public Particle P2 => m_Particle2;
        public float Length {  get; private set; }
        public LineRenderer LineRenderer { get; private set; }
        public float MaxLength => Length * m_LengthMultiplier;

        public void OnCreate(Particle p1, Particle p2, float lengthMultiplier)
        {
            m_Particle1 = p1;
            m_Particle2 = p2;
            m_LengthMultiplier = lengthMultiplier;
        }

        public void Init() {
            Length = (P1.transform.position - P2.transform.position).magnitude * m_LengthMultiplier;
        }

        public bool ContainsParticle( Particle p)
        {
            return p == m_Particle1 || p == m_Particle2;
        }
    }
}
