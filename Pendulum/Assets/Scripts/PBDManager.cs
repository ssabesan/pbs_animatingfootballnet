using System;
using System.Threading.Tasks;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Jobs;

namespace PhysicalBasedSimulation
{
    [BurstCompile]
    public class PBDManager : MonoBehaviour
    {
        [Header("Main PBD Paramter")]
        [SerializeField, Range(0, 10)]
        private float m_simulationSpeed = 1.0f;
        [SerializeField, Range(1, 10)]
        private int m_numSubSteps = 1;
        [SerializeField, Range(0, 1)]
        private float k_damping = 0.05f, k_stretching = 0.9f;
        [SerializeField]

        [Header("Fix Point Parameter")]
        private Transform[] m_FixedPoints;
        [SerializeField, Range(0.1f, 1.0f)]
        public float m_FixedPointMaxDistance = 0.1f;

        [Header("Simulation Time Paramters")]
        [SerializeField]
        private bool m_UseConstantDeltaTime = false;
        [SerializeField]
        private float m_ConstantDeltaTime = 0.01f, m_MaxDeltaTime = 0.01f;
        [SerializeField, Range(0, 100)]
        private float m_SimulationDelayInSeconds = 1.0f;
        public float SimulationDelayInSeconds => m_SimulationDelayInSeconds;

        // 3D Model parameters
        [Header("Paramters for 3D model")]
        [SerializeField]
        private bool m_use3DModel;
        [SerializeField, Range(1, 100)]
        private float m_MeshMass = 1.5f;
        private Mesh m_Mesh;

        [Header("Parameters used for ball-net interaction")]
        [SerializeField]
        public PBDBall m_PBDBall;
        [SerializeField, Range(0, 1.0f)]
        public float m_LengthMultiplier = 1.0f;
        [SerializeField, Range(0, 10.0f)]
        private float m_CollisionVertexMinDistance = 0.1f;

        // Particle Paramters
        private int ParticlesCount = 0;
        private NativeArray<float> ParticleMasses;
        private NativeArray<bool> ParticleIsFixed;
        private NativeArray<float3> ParticlePositions, ParticlePrevPositions, ParticleVelocities;
        private NativeArray<float3> ParticleCollisionForces;
        private Transform[] ParticleTransforms;
        private TransformAccessArray m_AccessArray;

        // Connection Paramters
        private int ConnectionsCount = 0;
        private NativeArray<int2> ConnectionIndices;
        private NativeArray<float> ConnectionLengths;

        // Other parameters
        private float totalMass = 0;
        private const float eps = 1e-5f;
        private static readonly float3x3 eps_3x3 = new(eps, eps, eps, eps, eps, eps, eps, eps, eps);
        public static float3 Gravity { get; private set; } = new(0, -9.81f, 0);
        private float m_SimulationTime = 0.0f;

        // Start is called before the first frame update
        void Start()
        {
            int numFixedPoints = m_FixedPoints.Length;
            NativeArray<float3> fixedPointPosition = new(numFixedPoints, Allocator.TempJob);
            for (int i = 0; i < numFixedPoints; i++)
            {
                fixedPointPosition[i] = m_FixedPoints[i].position;
            }

            if (m_use3DModel)
            {
                m_Mesh = gameObject.GetComponent<MeshFilter>().mesh;
                Vector3[] vertices = m_Mesh.vertices;
                int[] triangles = m_Mesh.triangles;

                ParticlesCount = vertices.Length;
                ConnectionsCount = triangles.Length;
                Debug.Log("Particles: " + ParticlesCount);
                Debug.Log("Connections: " + ConnectionsCount);

                // Particle Paramters
                ParticleTransforms = new Transform[ParticlesCount];
                ParticleMasses = new(ParticlesCount, Allocator.Persistent);
                ParticleIsFixed = new(ParticlesCount, Allocator.Persistent);
                ParticlePositions = new(ParticlesCount, Allocator.Persistent);
                ParticlePrevPositions = new(ParticlesCount, Allocator.Persistent);
                ParticleVelocities = new(ParticlesCount, Allocator.Persistent);
                ParticleCollisionForces = new(ParticlesCount, Allocator.Persistent);

                // Connection Paramters
                ConnectionIndices = new(ConnectionsCount, Allocator.Persistent);
                ConnectionLengths = new(ConnectionsCount, Allocator.Persistent);

                float mass = m_MeshMass / ParticlesCount;
                Matrix4x4 localToWorld = transform.localToWorldMatrix;

                Gravity = transform.worldToLocalMatrix.MultiplyPoint3x4(Gravity);
                Parallel.For(0, ParticlesCount, i => {
                    totalMass += mass;
                    ParticleMasses[i] = mass;
                    ParticleIsFixed[i] = false;
                    ParticlePositions[i] = vertices[i];
                    ParticlePrevPositions[i] = vertices[i];
                    ParticleVelocities[i] = float3.zero;
                    ParticleCollisionForces[i] = float3.zero;

                    bool isFixed = false;
                    float3 x = localToWorld.MultiplyPoint3x4(vertices[i]);
                    float minDist = Mathf.Infinity;
                    for (int j = 0; j < numFixedPoints; j++)
                    {
                        if (math.length(x - fixedPointPosition[j]) < m_FixedPointMaxDistance)
                        {
                            isFixed = true;
                            minDist = math.length(x - fixedPointPosition[j]);
                            break;
                        }
                    }

                    Bounds frame1 = new(new Vector3(-0.18f, 1f, 0f), new Vector3(1, 3, 7));
                    Bounds frame2 = new(new Vector3(1.454f, -1.018f, 0f), new Vector3(3, 1, 7));
                    minDist = math.min(math.sqrt(frame1.SqrDistance(x)), minDist);

                    if (frame1.Contains(x) || frame2.Contains(x))
                    {
                        isFixed = true;
                    }

                    ParticleIsFixed[i] = isFixed;
                    if(isFixed)
                    {
                        ParticleMasses[i] = mass / (minDist + eps);
                    }
                    totalMass += ParticleMasses[i];
                });

                Parallel.For(0, ConnectionsCount / 3, i => {
                    int j = i * 3;
                    int i1 = triangles[j];
                    int i2 = triangles[j + 1];
                    int i3 = triangles[j + 2];
                    float3 v1 = vertices[i1];
                    float3 v2 = vertices[i2];
                    float3 v3 = vertices[i3];

                    ConnectionIndices[j] = new int2(i1, i2);
                    ConnectionIndices[j + 1] = new int2(i1, i3);
                    ConnectionIndices[j + 2] = new int2(i2, i3);

                    ConnectionLengths[j] = m_LengthMultiplier * math.distance(v1, v2);
                    ConnectionLengths[j + 1] = m_LengthMultiplier * math.distance(v1, v3);
                    ConnectionLengths[j + 2] = m_LengthMultiplier * math.distance(v2, v3);
                });
            }
            else
            {
                Particle[] particles = gameObject.GetComponentsInChildren<Particle>();
                ParticleConnection[] connections = gameObject.GetComponentsInChildren<ParticleConnection>();
                ParticlesCount = particles.Length;
                ConnectionsCount = connections.Length;

                // Particle Paramters
                ParticleTransforms = new Transform[ParticlesCount];
                ParticleMasses = new(ParticlesCount, Allocator.Persistent);
                ParticleIsFixed = new(ParticlesCount, Allocator.Persistent);
                ParticlePositions = new(ParticlesCount, Allocator.Persistent);
                ParticlePrevPositions = new(ParticlesCount, Allocator.Persistent);
                ParticleVelocities = new(ParticlesCount, Allocator.Persistent);
                ParticleCollisionForces = new(ParticlesCount, Allocator.Persistent);

                // Connection Paramters
                ConnectionIndices = new(ConnectionsCount, Allocator.Persistent);
                ConnectionLengths = new(ConnectionsCount, Allocator.Persistent);

                float mass = m_MeshMass / ParticlesCount;
                for (int i = 0; i < particles.Length; i++)
                {
                    totalMass += mass;

                    ParticleTransforms[i] = particles[i].transform;
                    ParticleMasses[i] = mass;
                    ParticleIsFixed[i] = particles[i].IsFixed;
                    ParticlePositions[i] = particles[i].transform.position;
                    ParticlePrevPositions[i] = particles[i].transform.position;
                    ParticleVelocities[i] = float3.zero;
                    ParticleCollisionForces[i] = float3.zero;
                }
                m_AccessArray = new(ParticleTransforms);

                for (int i = 0; i < connections.Length; i++)
                {
                    connections[i].Init();

                    int2 indices = int2.zero;
                    bool foundP1 = false, foundP2 = false;
                    for (int j = 0; j < particles.Length; j++)
                    {
                        if (!foundP1 && particles[j] == connections[i].P1)
                        {
                            indices.x = j;
                            foundP1 = true;
                            if (foundP2)
                                break;
                        }
                        else if (!foundP2 && particles[j] == connections[i].P2)
                        {
                            indices.y = j;
                            foundP2 = true;
                            if (foundP1)
                                break;
                        }
                    }
                    if (!foundP1 || !foundP2)
                    {
                        Debug.Log("Connection not found");
                    }
                    ConnectionIndices[i] = indices;
                    ConnectionLengths[i] = m_LengthMultiplier * connections[i].Length;
                }
            }
        }

        private void OnDestroy()
        {
            if (m_AccessArray.isCreated)
                m_AccessArray.Dispose();

            if (ParticleMasses.IsCreated)
                ParticleMasses.Dispose();
            if (ParticleIsFixed.IsCreated)
                ParticleIsFixed.Dispose();
            if (ParticlePositions.IsCreated)
                ParticlePositions.Dispose();
            if (ParticlePrevPositions.IsCreated)
                ParticlePrevPositions.Dispose();
            if (ParticleVelocities.IsCreated)
                ParticleVelocities.Dispose();

            if (ConnectionIndices.IsCreated)
                ConnectionIndices.Dispose();
            if (ConnectionLengths.IsCreated)
                ConnectionLengths.Dispose();
        }

        // Update is called once per frame
        void Update()
        {
            m_SimulationTime += Time.deltaTime;
            if (m_SimulationTime < m_SimulationDelayInSeconds)
                return;

            // Clamp simulation speed to avoid extrem values, which can lead to problems.
            float dt = m_UseConstantDeltaTime ? m_ConstantDeltaTime : m_simulationSpeed * Time.smoothDeltaTime;
            dt = Mathf.Clamp(dt, 0.000001f, m_MaxDeltaTime);
            float sdt = dt / m_numSubSteps;
            for (int step = 0; step < m_numSubSteps; step++)
            {
                // Simulation
                SimulatePBD(sdt);
            }


            // Fix velocity to particles position change consistent
            if (m_use3DModel)
            {
                m_Mesh.SetVertices(ParticlePositions);
            }
            else
            {
                UpdateParticleJob updateParticleJob = new() {
                    ParticlePositions = ParticlePositions,
                };
                JobHandle updateParticleJobHandle = updateParticleJob.Schedule(m_AccessArray);
                updateParticleJobHandle.Complete();
                //for(int i = 0; i < ParticlesCount; i++) {
                //    ParticleTransforms[i].position = ParticlePositions[i];
                //}
            }
            if (m_PBDBall != null)
                m_PBDBall.UpdateTransformPosition();
        }

        // Physical Based Dynamics
        void SimulatePBD(float dt)
        {
            // Force is cutrrently only gravity but can be changed later for other source of force.
            float3 force = dt * Gravity;
            float3 X = float3.zero, V = float3.zero, W;

            // Compute velocity Damping
            NativeArray<float3> xArr = new(ParticlesCount, Allocator.TempJob);
            NativeArray<float3> vArr = new(ParticlesCount, Allocator.TempJob);

            // Update velocity with damping.
            ParticleAcc1Job particleAccJob = new()
            {
                // Read Data
                DeltaTime = dt,
                Masses = ParticleMasses,
                Positions = ParticlePositions,
                PrevPositions = ParticlePrevPositions,
                // Read & Write Data
                Velocities = ParticleVelocities,
                xArr = xArr,
                vArr = vArr,
            };
            JobHandle particleAccJobHandle = particleAccJob.Schedule(ParticlesCount, 64);
            particleAccJobHandle.Complete();
            for (int i = 0; i < ParticlesCount; i++)
            {
                X += xArr[i];
                V += vArr[i];
            }

            // Apply velocity Damping
            X /= totalMass;
            V /= totalMass;
            float3 L = float3.zero;
            float3x3 I = float3x3.zero;
            NativeArray<float3> LArr = new(ParticlesCount, Allocator.TempJob);
            NativeArray<float3x3> IArr = new(ParticlesCount, Allocator.TempJob);
            ParticleAcc2Job particleAcc2Job = new()
            {
                // Read Data
                X = X,
                ParticleMasses = ParticleMasses,
                ParticlePositions = ParticlePositions,
                ParticleVelocities = ParticleVelocities,
                // Read & Write Data
                IArr = IArr,
                LArr = LArr,
            };
            JobHandle particleAcc2JobHandle = particleAcc2Job.Schedule(ParticlesCount, 64);
            particleAcc2JobHandle.Complete();
            for (int i = 0; i < ParticlesCount; i++)
            {
                L += LArr[i];
                I += IArr[i];
            }
            I += eps_3x3;
            W = math.mul(math.inverse(I), L);

            xArr.Dispose();
            vArr.Dispose();
            IArr.Dispose();
            LArr.Dispose();

            // Update velocity with damping.
            ParticleVelocityJob particleVelocityJob = new() {
                // Read Data
                DeltaTime = dt,
                Damping = k_damping,
                Gravity = force,
                CollisionForce = ParticleCollisionForces,
                X = X,
                V = V,
                W = W,
                Masses = ParticleMasses,
                IsFixed = ParticleIsFixed,
                // Read & Write Data
                Positions = ParticlePositions,
                PrevPositions = ParticlePrevPositions,
                Velocities = ParticleVelocities,
            };
            JobHandle particleVelocityJobHandle = particleVelocityJob.Schedule(ParticlesCount, 64);

            if (m_PBDBall != null)
                m_PBDBall.UpdatePosition(dt);
            particleVelocityJobHandle.Complete();

            // Fix positions to satify constraints
            ConnectionConstraintJob connectionConstraintJob = new() {
                // Read Data
                DeltaTime = dt,
                Stretching = k_stretching,
                Masses = ParticleMasses,
                IsFixed = ParticleIsFixed,
                ConnectionIndices = ConnectionIndices,
                ConnectionLengths = ConnectionLengths,
                // Read & Write Data
                Positions = ParticlePositions,
            };
            JobHandle connectionConstraintJobHandle = connectionConstraintJob.Schedule(ConnectionsCount, 64);
            connectionConstraintJobHandle.Complete();

            if (m_PBDBall != null)
                CheckForCollision(m_PBDBall);
        }

        public void CheckForCollision(PBDBall ball)
        {
            // convert point to local space
            Matrix4x4 localToWorld = transform.localToWorldMatrix;
            Matrix4x4 worldToLocal = transform.worldToLocalMatrix;

            // scan all vertices to find nearest
            for (int i = 0; i < ParticlesCount; i++)
            {
                int p1 = i;
                float p1Weight = (!ParticleIsFixed[p1] && ParticleMasses[p1] > 0.0f) ? 1.0f / ParticleMasses[p1] : 0.0f;
                float p2Weight = (!ball.m_isFixed && ball.Mass > 0.0f) ? 1.0f / ball.Mass : 0.0f;
                float sumWeight = p1Weight + p2Weight;

                if (sumWeight <= 0)
                    return;

                // Rectifying constraint violations
                float3 p1Pos = localToWorld.MultiplyPoint3x4(ParticlePositions[p1]);
                float3 p2Pos = m_PBDBall.Position;

                float3 currLengthVec = p1Pos - p2Pos;
                float cosTheta = math.max(0, math.dot(ball.Velocity, math.normalize(currLengthVec)));
                float currLength = math.length(currLengthVec);
                float corr = (currLength - ball.Radius - cosTheta * m_CollisionVertexMinDistance) / currLength;

                if (corr > 0.0f)
                    continue;
                p1Pos -= (p1Weight / sumWeight) * corr * currLengthVec;
                p2Pos += (p2Weight / sumWeight) * corr * currLengthVec;

                ParticlePositions[p1] = worldToLocal.MultiplyPoint3x4(p1Pos);
                m_PBDBall.Position = p2Pos;
            }
        }
    }
}
