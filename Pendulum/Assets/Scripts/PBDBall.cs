using PhysicalBasedSimulation;
using Unity.Mathematics;
using UnityEngine;

public class PBDBall : MonoBehaviour
{
    [SerializeField]
    private PBDManager manager;
    [SerializeField]
    private float3 m_ConstantForce;
    [SerializeField]
    private bool useGravity;
    [SerializeField]
    public bool m_isFixed;
    [SerializeField]
    private float m_GroundCollisionFactor;

    // Particle Paramters
    public float Mass;
    [HideInInspector]
    public float3 Position, PrevPosition, Velocity, m_CollisionImpluse, m_InitialImpulse, RotationAngles; 
    [HideInInspector]
    public float Radius { get; private set; }

    // Other parameters
    public static float3 Gravity { get; private set; } = new(0, -9.81f, 0);

    public void InitBall()
    {
        Radius = transform.lossyScale.x / 2f;
        Position = transform.position;
        PrevPosition = transform.position;
        Velocity = float3.zero;
    }

    public void UpdatePosition(float DeltaTime)
    {
        float3 delta = Position - PrevPosition;
        Velocity = delta / DeltaTime;
        float3 gravityForce = useGravity ? Gravity : float3.zero;

        // Update velocity with damping.
        PrevPosition = Position;
        if (!m_isFixed)
        {   
            Velocity += DeltaTime * (gravityForce + m_ConstantForce) + m_CollisionImpluse + m_InitialImpulse + (float3)GetComponent<Rigidbody>().velocity;
            m_InitialImpulse = float3.zero;
            m_CollisionImpluse = float3.zero;

            // Update position and possibly violate constraints.
            Position += Velocity * DeltaTime;

        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision == null) return;
        m_CollisionImpluse = collision.contacts[0].normal * -Velocity * m_GroundCollisionFactor;
        RotationAngles = m_CollisionImpluse;
        RotationAngles *= 0.45f;
    }

    public void UpdateTransformPosition()
    {   
        //print((Vector3)Position-transform.position);
        transform.position = Position;
        transform.Rotate(RotationAngles, Space.Self);
    }
}
