using PhysicalBasedSimulation;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    public PBDBall ball;
    private float time;
    private PBDBall cur;
    public PBDManager manager;
    public float minForcef = 100f;
    public float maxForcef = 1000f;
    public float minForceh = -10f;
    public float maxForceh = 10f;
    public float minForcev = 0f;
    public float maxForcev = 20f;
    public float maxTime = 20f;
    // Start is called before the first frame update
    void Start()
    {
        cur = Instantiate(ball, transform.position, transform.rotation);
        cur.InitBall();
        cur.m_InitialImpulse = (new Vector3(UnityEngine.Random.Range(minForcef, maxForcef), UnityEngine.Random.Range(minForcev, maxForcev), UnityEngine.Random.Range(minForceh, maxForceh)));
        manager.m_PBDBall = cur;
        time = Time.time;

    }

    // Update is called once per frame
    void Update()
    {
        if (time - Time.time < -maxTime)
        {   
            if(cur != null)
            {
                Destroy(cur.gameObject);
            }
            cur = Instantiate(ball, transform.position, transform.rotation);
            cur.InitBall();
            cur.m_InitialImpulse = (new Vector3(UnityEngine.Random.Range(minForcef, maxForcef), UnityEngine.Random.Range(minForcev, maxForcev), UnityEngine.Random.Range(minForceh, maxForceh)));
            cur.RotationAngles = (new Vector3(0, 0, -10));
            manager.m_PBDBall = cur;
            time = Time.time;
        }
    }
}
