using System.Collections.Generic;
using UnityEngine;

/// Old Implementation of PBD and analytical Pendulum
/// Could be used to validate that our current implementation is still working as intended, i.e. correctly.

namespace PhysicalBasedSimulation.Old
{
    public class Pendulum : MonoBehaviour
    {
        [SerializeField, Range(0, 10)]
        private float m_simulationSpeed = 1.0f;
        [SerializeField, Range(1, 10)]
        private int m_numSubSteps = 1;
        [SerializeField]
        private bool m_usePBD;
        [SerializeField]
        private Color m_color;
        [SerializeField]
        private Particle[] particles;

        private readonly List<float> theta = new();
        private readonly List<float> omega = new();

        // Start is called before the first frame update
        void Start()
        {
            theta.Add(0.0f);
            omega.Add(0.0f);

            Vector3 down = Vector3.down;
            for (int i = 1; i < particles.Length; i++) {
                Particle particle = particles[i];
                particle.Init();

                // Parameters needed for analytical solution
                Vector3 parentToChild = (particle.transform.position - particle.Parent.transform.position).normalized;
                float angle = Mathf.Deg2Rad * Vector3.Angle(down, parentToChild);
                theta.Add(angle);
                omega.Add(0.0f);

                // Visualization stuff
                LineRenderer lr = particle.GetComponent<LineRenderer>();
                lr.SetPosition(0, particle.Parent.transform.position);
                lr.SetPosition(1, particle.transform.position);
                lr.startColor = m_color;
                lr.endColor = m_color;
            }
        }

        // Update is called once per frame
        void Update()
        {
            // Clamp simulation speed to avoid extrem values, which can lead to problems.
            float dt = m_simulationSpeed * Time.smoothDeltaTime;
            if(dt > 0.2f) {
                Debug.Log($"Extrem warning!: dt = {dt}");
            }
            dt = Mathf.Clamp(dt, 0.000001f, 0.2f);

            float sdt = dt / m_numSubSteps;
            for (int step = 0; step < m_numSubSteps; step++) {
                // Simulation
                if (m_usePBD) {
                    SimulatePBD(sdt, -9.81f);
                } else {
                    SimulateAnalytic(sdt, -9.81f);
                }
            }
        }

        // Physical Based Dynamics
        void SimulatePBD(float dt, float gravity)
        {
            // Force is cutrrently only gravity but can be changed later for other source of force.
            Vector3 force = new(0, dt * gravity, 0);

            // Update and possible violate constraints.
            for (int i = 1; i < particles.Length; i++) {
                Particle particle = particles[i];
                particle.Veclotiy += force;
                particle.PrevPosition = particle.transform.position;
                particle.transform.position += particle.Veclotiy * dt;
            }

            // Fix positions to satify constraints
            for (int i = 1; i < particles.Length; i++) {
                Particle particle = particles[i];
                Vector3 childPos = particle.transform.position;
                Vector3 parentPos = particle.Parent.transform.position;

                // Rectifying constraint violations
                Vector3 currLengthVec = childPos - parentPos;
                float currLength = currLengthVec.magnitude;

                float weightParent = particle.Parent.Mass > 0.0f ? 1.0f / particle.Parent.Mass : 0.0f;
                float wegihtChild = particle.Mass > 0.0f ? 1.0f / particle.Mass : 0.0f;
                float corr = (particle.Length - currLength) / currLength / (weightParent + wegihtChild);

                parentPos -= weightParent * corr * currLengthVec;
                childPos += wegihtChild * corr * currLengthVec;

                // Updating positions and rendering of connections
                LineRenderer lr = particle.GetComponent<LineRenderer>();
                lr.SetPosition(0, parentPos);
                lr.SetPosition(1, childPos);

                particle.transform.position = childPos;
                particle.Parent.transform.position = parentPos;
            }

            // Fix velocity to keep position change consistent
            for (int i = 1; i < particles.Length; i++) {
                particles[i].UpdateVelocity(dt);
            }
        }

        // Analytical only works for 2D (Might need another angle for 3D)
        void SimulateAnalytic(float dt, float gravity)
        {
            float g = -gravity;
            float m1 = particles[1].Mass;
            float m2 = particles[2].Mass;
            float m3 = particles[3].Mass;
            float l1 = particles[1].Length;
            float l2 = particles[2].Length;
            float l3 = particles[3].Length;
            float t1 = theta[1];
            float t2 = theta[2];
            float t3 = theta[3];
            float w1 = omega[1];
            float w2 = omega[2];
            float w3 = omega[3];

            float b1 = g * l1 * m1 * Mathf.Sin(t1) + g * l1 * m2 * Mathf.Sin(t1) + g * l1 * m3 * Mathf.Sin(t1) + m2 * l1 * l2 * Mathf.Sin(t1 - t2) * w1 * w2 +
                    m3 * l1 * l3 * Mathf.Sin(t1 - t3) * w1 * w3 + m3 * l1 * l2 * Mathf.Sin(t1 - t2) * w1 * w2 +
                    m2 * l1 * l2 * Mathf.Sin(t2 - t1) * (w1 - w2) * w2 +
                    m3 * l1 * l2 * Mathf.Sin(t2 - t1) * (w1 - w2) * w2 +
                    m3 * l1 * l3 * Mathf.Sin(t3 - t1) * (w1 - w3) * w3;

            float a11 = l1 * l1 * (m1 + m2 + m3);
            float a12 = m2 * l1 * l2 * Mathf.Cos(t1 - t2) + m3 * l1 * l2 * Mathf.Cos(t1 - t2);
            float a13 = m3 * l1 * l3 * Mathf.Cos(t1 - t3);

            float b2 =
                g * l2 * m2 * Mathf.Sin(t2) + g * l2 * m3 * Mathf.Sin(t2) + w1 * w2 * l1 * l2 * Mathf.Sin(t2 - t1) * (m2 + m3) +
                m3 * l2 * l3 * Mathf.Sin(t2 - t3) * w2 * w3 +
                (m2 + m3) * l1 * l2 * Mathf.Sin(t2 - t1) * (w1 - w2) * w1 +
                m3 * l2 * l3 * Mathf.Sin(t3 - t2) * (w2 - w3) * w3;

            float a21 = (m2 + m3) * l1 * l2 * Mathf.Cos(t2 - t1);
            float a22 = l2 * l2 * (m2 + m3);
            float a23 = m3 * l2 * l3 * Mathf.Cos(t2 - t3);

            float b3 =
                m3 * g * l3 * Mathf.Sin(t3) - m3 * l2 * l3 * Mathf.Sin(t2 - t3) * w2 * w3 - m3 * l1 * l3 * Mathf.Sin(t1 - t3) * w1 * w3 +
                m3 * l1 * l3 * Mathf.Sin(t3 - t1) * (w1 - w3) * w1 +
                m3 * l2 * l3 * Mathf.Sin(t3 - t2) * (w2 - w3) * w2;

            float a31 = m3 * l1 * l3 * Mathf.Cos(t1 - t3);
            float a32 = m3 * l2 * l3 * Mathf.Cos(t2 - t3);
            float a33 = m3 * l3 * l3;

            b1 = -b1;
            b2 = -b2;
            b3 = -b3;

            float det = a11 * (a22 * a33 - a23 * a32) + a21 * (a32 * a13 - a33 * a12) + a31 * (a12 * a23 - a13 * a22);
            if (det == 0.0) {
                Debug.Log("Determinant is zero!");
                return;
            }

            float a1 = b1 * (a22 * a33 - a23 * a32) + b2 * (a32 * a13 - a33 * a12) + b3 * (a12 * a23 - a13 * a22);
            float a2 = b1 * (a23 * a31 - a21 * a33) + b2 * (a33 * a11 - a31 * a13) + b3 * (a13 * a21 - a11 * a23);
            float a3 = b1 * (a21 * a32 - a22 * a31) + b2 * (a31 * a12 - a32 * a11) + b3 * (a11 * a22 - a12 * a21);

            a1 /= det;
            a2 /= det;
            a3 /= det;

            omega[1] += a1 * dt;
            omega[2] += a2 * dt;
            omega[3] += a3 * dt;
            theta[1] += omega[1] * dt;
            theta[2] += omega[2] * dt;
            theta[3] += omega[3] * dt;

            for (int i = 1; i < 4; i++) {
                float particleLength = particles[i].Length;
                Vector3 parentPos = particles[i].Parent.transform.position;
                Vector3 pos = parentPos + new Vector3(particleLength * Mathf.Sin(theta[i]), particleLength * -Mathf.Cos(theta[i]), 0);
                particles[i].transform.position = pos;

                LineRenderer lr = particles[i].GetComponent<LineRenderer>();
                lr.SetPosition(0, parentPos);
                lr.SetPosition(1, pos);
            }
        }
    }
}
