using UnityEngine;

/// Old Implementation of Particle which is used by the old pendulum system.
/// Could be used to validate that our current implementation is still working as intended, i.e. correctly.

namespace PhysicalBasedSimulation.Old
{
    public class Particle : MonoBehaviour
    {
        // Private fields (in Inspector)
        [SerializeField]
        private float m_Mass = 1.0f;
        [SerializeField]
        private bool m_isFixed = false;
        [SerializeField]
        private Particle m_Parent;
        
        // Getters of private fields in Inspector
        public float Mass => m_Mass;
        public bool IsFixed => m_isFixed;
        public Particle Parent => m_Parent;

        // Public fields
        public Vector3 PrevPosition { get; set; }
        public Vector3 Veclotiy { get; set; }
        public float Length { get; private set; }

        public void Init()
        {
            PrevPosition = transform.position;
            Veclotiy = Vector3.zero;
            Length = m_Parent == null ? 0 : (m_Parent.transform.position - transform.position).magnitude;
        }

        public void UpdateVelocity(float dt)
        {
            Vector3 delta = transform.position - PrevPosition;
            Veclotiy = delta / dt;
        }
    }
}
