﻿using UnityEngine;

public class FPSDisplay : MonoBehaviour
{
    [SerializeField]
    private Color m_Color = new Color(0.5f, 0.0f, 0.0f, 1.0f);
    private float deltaTime = 0.0f;

    // Update
    void Update()
    {
        deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
    }

    // On GUI
    void OnGUI()
    {
        int w = Screen.width, h = Screen.height;

        GUIStyle style = new();

        Rect rect = new(0, 0, w, h * 2 / 100);
        style.alignment = TextAnchor.UpperRight;
        style.fontSize = h * 3 / 100;
        style.normal.textColor = m_Color;
        float msec = deltaTime * 1000.0f;
        float fps = 1.0f / deltaTime;
        string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
        GUI.Label(rect, text, style);
    }
}

