using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine.Jobs;

namespace PhysicalBasedSimulation
{
    [BurstCompile]
    public struct ParticleVelocityJob : IJobParallelFor
    {
        [ReadOnly] public float DeltaTime, Damping;
        [ReadOnly] public float3 Gravity;
        [ReadOnly] public float3 X, V, W;

        [ReadOnly] public NativeArray<float> Masses;
        [ReadOnly] public NativeArray<bool> IsFixed;

        public NativeArray<float3> Positions;
        public NativeArray<float3> PrevPositions;
        public NativeArray<float3> Velocities;
        public NativeArray<float3> CollisionForce;

        public void Execute(int index)
        {
            // Update velocity with damping.
            float3 delta = Positions[index] - PrevPositions[index];
            Velocities[index] = delta / DeltaTime;

            PrevPositions[index] = Positions[index];
            if (!IsFixed[index])
            {
                Velocities[index] += Gravity + CollisionForce[index];
                CollisionForce[index] = float3.zero; ;
                float3 r = Positions[index] - X;
                float3 deltav = V + math.cross(W, r) - Velocities[index];
                Velocities[index] += Damping * deltav;

                // Update position and possibly violate constraints.
                Positions[index] += Velocities[index] * DeltaTime;
            }
        }
    }

    [BurstCompile]
    public struct ParticleAcc1Job : IJobParallelFor
    {
        [ReadOnly] public float DeltaTime;

        [ReadOnly] public NativeArray<float> Masses;
        [ReadOnly] public NativeArray<float3> Positions;
        [ReadOnly] public NativeArray<float3> PrevPositions;

        //[WriteOnly] [ReadOnly]
        public NativeArray<float3> Velocities;
        [WriteOnly] public NativeArray<float3> xArr;
        [WriteOnly] public NativeArray<float3> vArr;

        public void Execute(int index)
        {
            float3 delta = Positions[index] - PrevPositions[index];
            float3 vel = delta / DeltaTime;

            Velocities[index] = vel;
            xArr[index] = Positions[index] * Masses[index];
            vArr[index] = vel * Masses[index];
        }
    }

    [BurstCompile]
    public struct ParticleAcc2Job : IJobParallelFor
    {
        [ReadOnly] public float3 X;

        [ReadOnly] public NativeArray<float> ParticleMasses;
        [ReadOnly] public NativeArray<float3> ParticlePositions;
        [ReadOnly] public NativeArray<float3> ParticleVelocities;

        [WriteOnly] public NativeArray<float3x3> IArr;
        [WriteOnly] public NativeArray<float3> LArr;

        public void Execute(int index)
        {
            float3 r = ParticlePositions[index] - X;
            LArr[index] = math.cross(r, ParticleMasses[index] * ParticleVelocities[index]);
            float3x3 r_wave = new(
                0, -r[2], r[1],
                r[2], 0, -r[0],
                -r[1], r[0], 0
            );
            float3x3 r_r_transpose = math.mul(r_wave, math.transpose(r_wave));
            IArr[index] = r_r_transpose * ParticleMasses[index];
        }
    }

    [BurstCompile]
    public struct UpdateParticleJob : IJobParallelForTransform
    {
        [ReadOnly] public NativeArray<float3> ParticlePositions;
        public void Execute(int index, TransformAccess transform)
        {
            transform.position = ParticlePositions[index];
        }
    }
}
