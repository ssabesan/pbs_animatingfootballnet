using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;

namespace PhysicalBasedSimulation
{
    [BurstCompile]
    public struct ConnectionConstraintJob : IJobParallelFor
    {
        // Hyper Paramters
        [ReadOnly] public float DeltaTime, Stretching;

        // Particle Paramters
        [ReadOnly] public NativeArray<float> Masses;
        [ReadOnly] public NativeArray<bool> IsFixed;

        [NativeDisableParallelForRestriction]
        public NativeArray<float3> Positions;

        // Connections Paramters
        [ReadOnly] public NativeArray<int2> ConnectionIndices;
        [ReadOnly] public NativeArray<float> ConnectionLengths;

        public void Execute(int index)
        {
            int p1 = ConnectionIndices[index].x;
            int p2 = ConnectionIndices[index].y;

            float p1Weight = (!IsFixed[p1] && Masses[p1] > 0.0f) ? 1.0f / Masses[p1] : 0.0f;
            float p2Weight = (!IsFixed[p2] && Masses[p2] > 0.0f) ? 1.0f / Masses[p2] : 0.0f;
            float sumWeight = p1Weight + p2Weight;

            if (sumWeight <= 0)
                return;

            // Rectifying constraint violations
            float3 p1Pos = Positions[p1];
            float3 p2Pos = Positions[p2];

            float3 currLengthVec = p1Pos - p2Pos;
            float currLength = math.length(currLengthVec);
            float corr = (currLength - ConnectionLengths[index]) / currLength * Stretching;

            p1Pos -= (p1Weight / sumWeight) * corr * currLengthVec;
            p2Pos += (p2Weight / sumWeight) * corr * currLengthVec;

            // Updating positions and rendering of connections
            Positions[p1] = p1Pos;
            Positions[p2] = p2Pos;
        }
    }

    public struct BallConstraintJob : IJobParallelFor
    {
        // Hyper Paramters
        [ReadOnly] public float DeltaTime, Stretching;

        // Particle Paramters
        [ReadOnly] public NativeArray<float> Masses;
        [ReadOnly] public NativeArray<bool> IsFixed;

        [NativeDisableParallelForRestriction]
        public NativeArray<float3> Positions;

        // Connections Paramters
        [ReadOnly] public NativeArray<int2> ConnectionIndices;
        [ReadOnly] public NativeArray<float> ConnectionLengths;

        public void Execute(int index)
        {
            int p1 = ConnectionIndices[index].x;
            int p2 = ConnectionIndices[index].y;

            float p1Weight = (!IsFixed[p1] && Masses[p1] > 0.0f) ? 1.0f / Masses[p1] : 0.0f;
            float p2Weight = (!IsFixed[p2] && Masses[p2] > 0.0f) ? 1.0f / Masses[p2] : 0.0f;
            float sumWeight = p1Weight + p2Weight;

            if (sumWeight <= 0)
                return;

            // Rectifying constraint violations
            float3 p1Pos = Positions[p1];
            float3 p2Pos = Positions[p2];

            float3 currLengthVec = p1Pos - p2Pos;
            float currLength = math.length(currLengthVec);
            float corr = (currLength - ConnectionLengths[index]) / currLength * Stretching;

            p1Pos -= (p1Weight / sumWeight) * corr * currLengthVec;
            p2Pos += (p2Weight / sumWeight) * corr * currLengthVec;

            // Updating positions and rendering of connections
            Positions[p1] = p1Pos;
            Positions[p2] = p2Pos;
        }
    }

}
