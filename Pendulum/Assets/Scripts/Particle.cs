using Unity.Burst;
using UnityEngine;

namespace PhysicalBasedSimulation
{
    [BurstCompile]
    public class Particle : MonoBehaviour
    {
        // Private fields (in Inspector)
        [SerializeField]
        private bool m_isFixed = false;
        
        // Getters of private fields in Inspector
        public bool IsFixed => m_isFixed;

        public void OnCreate(bool isFixed)
        {
            m_isFixed = isFixed;
        }
    }
}
