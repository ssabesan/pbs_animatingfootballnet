using PhysicalBasedSimulation;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(NetManager))]
public class NetManagerInspector : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        NetManager manager = (NetManager) target;

        if (GUILayout.Button("Generate new net")) {
            manager.GenerateNet();
        }
        if (GUILayout.Button("Override current net")) {
            manager.OverrideNet();
        }
        if (manager.PBDManager != null) {
            if (GUILayout.Button("Customize PBDManager Parameters")) {
                Selection.activeGameObject = manager.PBDManager.gameObject;
            }
        }
    }
}
