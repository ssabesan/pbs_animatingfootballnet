using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttatchPoint : MonoBehaviour
{
    public Transform pole;
    private LineRenderer lineRenderer;
    // Start is called before the first frame update
    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.SetPositions(new Vector3[2]{ pole.position + new Vector3(0,1.9f,0), transform.position });
    }

    // Update is called once per frame
    void Update()
    {
        lineRenderer.SetPositions(new Vector3[2] { pole.position + new Vector3(0, 1.9f, 0), transform.position });
    }
}
