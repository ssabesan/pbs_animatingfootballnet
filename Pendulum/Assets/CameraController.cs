using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Vector4[] locations;
    public Vector3 target;
    public float speed = 1;
    private float time;
    private int i = 0;
    private Vector3 direction;
    public float delay = 2, currTime;

    // Start is called before the first frame update
    void Start()
    {   
        time = Time.time;
        direction = (new Vector3(locations[i].x, locations[i].y, locations[i].z) - transform.position).normalized;
        currTime = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {   
        if (currTime < delay) {
            currTime += Time.deltaTime;
            return;
        }

        if (i < locations.Length)
        {
            transform.LookAt(target);
            if (Time.time - time >= locations[i].w)
            {
                i++;

                if (i >= locations.Length) return;
                time = Time.time;
                direction = (new Vector3(locations[i].x, locations[i].y, locations[i].z) - transform.position).normalized;
            }
            else if ((new Vector3(locations[i].x, locations[i].y, locations[i].z) - transform.position).magnitude > 5)
            {
                transform.position += direction * speed;
            }
            else if ((new Vector3(locations[i].x, locations[i].y, locations[i].z) - transform.position).magnitude > 1)
            {
                transform.position += direction * speed/2;
            }
        }
        
    }
}
