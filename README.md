# Introduction 
In this project we implemented Physical Based Dynamics motivated by a TenMinutePhysics [video](https://www.youtube.com/watch?v=XPZEeS70zzU&t=466s&ab_channel=TenMinutePhysics).  
[Here](https://gitlab.ethz.ch/ssabesan/pbs_animatingfootballnet) you can find the repository.

# How to run
When you pull this repo make sure to have git lfs installed, since otherwise, not all assets will be pulled correctly.  
To run the project just open it in the UnityHub with Editor Version 2022.3.12f1.  
There are 3 demo scenes where you can see some examples on how well our Physical Based Dynamics works:
- Pendulum: In this scene you can see us compare our implementation with the 2D analytical solution presented in the video.
- ParameterComp: In this scene you can see some comparisons between different parameter values applied to the net.
- FootballStadium: This is our final scene, which where we shoot multiple balls at different velocities at the goal and see some nice ball-net interactions.
